function krpipepictures()
  return
  {
    north =
    {
      filename = "__Krastorio-graphics__/graphics/entity/empty.png",
      priority = "high",
      width = 1,
      height = 1,
	  scale = 0.5,
      shift = {0, 0}
    },
    east =
    {
      filename = "__Krastorio-graphics__/graphics/entity/empty.png",
      priority = "high",
      width = 1,
      height = 1,
	  scale = 0.5,
      shift = {0, 0}
    },
    south =
    {
      filename = "__Krastorio-graphics__/graphics/entity/kr-pipe-straight-vertical.png",
      priority = "high",
      width = 23,
      height = 20,
      shift = {0.01, -0.6},
	  hr_version =
		{
		  filename = "__Krastorio-graphics__/graphics/entity/hr-kr-pipe-straight-vertical.png",
		  priority = "high",
		  width = 45,
		  height = 40,
		  scale = 0.5,
		  shift = {0.01, -0.6}
		}
    },
    west =
    {
      filename = "__Krastorio-graphics__/graphics/entity/empty.png",
      priority = "high",
      width = 1,
      height = 1,
	  scale = 0.5,
      shift = {0, 0}
    }
  }
end